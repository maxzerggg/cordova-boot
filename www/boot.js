(async()=>{
	"use strict";

	await ___LOAD_SCRIPT("./appStorage.js");

	// if first
	console.log("hi~");
	console.log(cordova);
	console.log(window);
	console.log(appStorage);


	await appStorage.init();
	const APP_STATUS = await appStorage.getItem("______appStatus");
	console.log("______appStatus: ", APP_STATUS);
	if ( APP_STATUS ) {
		window.location.href = `${cordova.file.dataDirectory}boot/index.html`;
		return;
	}


	await appStorage.setItem("______appStatus", JSON.stringify({a:1, b:2, c:456, d:""}));
	localStorage.setItem("______boot_root",	cordova.file.applicationDirectory + 'www/');


	const dirEntry = await ___GET_LOCAL_FILE_SYSTEM_URL(cordova.file.applicationDirectory);
	console.log("dirEntry: ", dirEntry);
	const fileEntry = await ___GET_FILE(dirEntry, '/www/fileList.json', {create:false, exclusive:false});
	console.log("fileEntry: ", fileEntry);
	const fileList = await ___READ_FILE(fileEntry);
	const {files} = JSON.parse(fileList);
	const dstDirEntry = await ___GET_LOCAL_FILE_SYSTEM_URL(cordova.file.dataDirectory);


	for(let {src, dst} of files) {
		let _fileEntry = await ___GET_FILE(dirEntry, src, {create:false, exclusive:false});
		console.log(`${src}: `, _fileEntry);
		let subdirEntry = await ___CREATE_DIR(dstDirEntry, dst);
		console.log(`${dst}: `, subdirEntry);
		_fileEntry.copyTo(subdirEntry, dst.split("/").splice(-1, 1)[0], ()=>{console.log(`${dst} file copy success`)});
	}




	window.location.href = `${cordova.file.dataDirectory}boot/index.html`;


	function ___CREATE_DIR (rootEntry, path) {
		return new Promise(async (resolve)=>{
			const dirs = path.split('/');
			if( dirs.length === 1 )		resolve(rootEntry);


			let newDirEntry = null;
			dirs.splice(-1, 1);// last one should be a file, not a directory, so delete it
			for(let dir of dirs) {
				newDirEntry = await ___GET_DIRECTORY(!newDirEntry ? rootEntry : newDirEntry, dir, {create:true, exclusive:false})
			}
			resolve(newDirEntry);
		});
	}
	function ___GET_FILE(rootEntry, dir, option) {
		return new Promise ((resolve)=>{
			rootEntry.getFile(dir, option, (newFileEntry)=>{ resolve(newFileEntry); }, (err)=>{	throw new Error(err); })
		});
	}
	function ___GET_DIRECTORY(rootEntry, dir, option) {
		return new Promise ((resolve)=>{
			rootEntry.getDirectory(dir, option, (newDirEntry)=>{resolve(newDirEntry);}, (err)=>{throw new Error(err);})
		});
	}
	function ___GET_LOCAL_FILE_SYSTEM_URL(path) {
		return new Promise((resolve)=>{
			window.resolveLocalFileSystemURL(path, (dirEntry)=>{
				resolve(dirEntry);
			}, (e)=>{ throw new Error(e); });
		});
	}
	function ___READ_FILE(fileEntry) {
		return new Promise((resolve)=>{
			fileEntry.file((file)=>{
				let reader = new FileReader();

				reader.onloadend = async function() {
					resolve(this.result);
				};

				reader.readAsText(file);
			}, (e)=>{throw new Error(e);});
		});
	}
	function ___LOAD_SCRIPT(path){
		return new Promise((resolve, reject)=>{
			let element = document.createElement( 'script' );
			element.type = "application/javascript";
			element.src = path;
			element.onload = resolve;
			element.onerror = reject;
			document.body.appendChild(element);
		});
	}
})();
