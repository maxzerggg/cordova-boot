

class appStorage {
	constructor() {}

	static async init() {
		return new Promise(async(resolve)=>{
			if( !window.cordova ) {
				throw new Error("cordova is undefined");
			}

			await ___SETTING();

			appStorage._cache = await ___READ_FILE();
			resolve();
		});
	}

	static setItem(key, value) {
		return new Promise(async(resolve)=>{
			if( !appStorage._fileEntry )	throw new Error("appStorage is not init");


			appStorage._cache[key] = value;
			await ___WRITE_FILE(appStorage._cache);
			resolve();
		});
	}

	static getItem(key=null) {
		return new Promise(async(resolve)=>{
			if( !appStorage._fileEntry )	throw new Error("appStorage is not init");
			if( key === null )				resolve(appStorage._cache);


			resolve(appStorage._cache[key]);
		});
	}
}



async function ___SETTING() {
	appStorage._dirEntry = await ___GET_LOCAL_FILE_SYSTEM_URL(cordova.file.dataDirectory);
	const isExist = await ___GET_FILE(appStorage._dirEntry, "___appStatus.json", {create:false, exclusive:false});
	if( !isExist ) {
		appStorage._fileEntry = await ___GET_FILE(appStorage._dirEntry, "___appStatus.json", {create:true, exclusive:false});
		await ___WRITE_FILE({});
	}else {
		appStorage._fileEntry = isExist;
	}

	function ___GET_LOCAL_FILE_SYSTEM_URL(path) {
		return new Promise((resolve)=>{
			window.resolveLocalFileSystemURL(path, (dirEntry)=>{
				resolve(dirEntry);
			}, (e)=>{ throw new Error(e); });
		});
	}
	function ___GET_FILE(dirEntry, fileName, option) {
		return new Promise ((resolve)=>{
			dirEntry.getFile(fileName, option, (fileEntry)=>{ resolve(fileEntry); }, (err)=>{
				switch (err.code) {
					case 1:
					{ resolve(null); }	// NOT_FOUND
						break;
					default:
					{ throw new Error(err); }
						break;
				}
			})
		});
	}
}
function ___READ_FILE() {
	return new Promise((resolve)=>{
		appStorage._fileEntry.file((file)=>{
			let reader = new FileReader();

			reader.onloadend = async function() {
				resolve(JSON.parse(this.result));
			};

			reader.readAsText(file);
		}, (e)=>{throw new Error(e);});
	});
}
function ___WRITE_FILE(dataObj) {
	return new Promise((resolve)=>{
		appStorage._fileEntry.createWriter(function (fileWriter) {

			fileWriter.onwriteend = function() {
				resolve();
			};

			fileWriter.onerror = function (e) {
				throw new Error(e);
			};

			fileWriter.write(JSON.stringify(dataObj));
		});
	});
}

window.___appStorage = appStorage;
